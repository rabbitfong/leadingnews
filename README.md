# Javascript ES6 + Webpack Kankan News 看看头条

#### 介绍
一个以原生Javascript(ES6)以及Webpack项目工程化构建的新闻头条项目，涉及功能模块化与项目组件化的前端架构，并运用前端数据缓存池技术进行数据的传递与存储使用。

#### 软件架构
Javascript(ES6) & AJAX & Sass & Webpack 
